"use strict";

// function fun(a, b) {
//   return a / b;
// }

// const num1 = 10;
// const num2 = 5;

// const result = fun(num1, num2);
// console.log(result);

// function userNumber(massege) {
//   let userInput;
//   do {
//     userInput = prompt(massege);
//   } while (isNaN(userInput));
//   return parseFloat(userInput);
// }

// const number1 = userNumber("Введіть перше число:");
// const number2 = userNumber("Введіть друге число:");

// const operation = prompt("Введіть операцію:");

// if (["+", "-", "*", "/"].includes(operation)) {
//   function calculate(num1, num2, op) {
//     switch (op) {
//       case "+":
//         return num1 + num2;
//       case "-":
//         return num1 - num2;
//       case "*":
//         return num1 * num2;
//       case "/":
//         if (num2 !== 0) {
//           return num1 / num2;
//         } else {
//           alert("Ділення на нуль неможливе.");
//           return NaN;
//         }
//       default:
//     }
//   }
//   const result = calculate(number1, number2, operation);
//   console.log(`${result}`);
// } else {
//   alert("Такої операції не існує.");
// }

const userNumber = prompt("Введіть число для розрахунку факторіалу:");
const inputNumber = userNumber;

const calculate = (number) => {
  let factorial = 1;

  for (let i = 1; i <= number; i++) {
    factorial *= i;
  }

  return factorial;
};

if (!isNaN(inputNumber)) {
  const result = calculate(inputNumber);
  alert(`${result}`);
} else {
  alert("Будь ласка, введіть коректне число.");
}
